import React from "react";
import { Link } from "react-router-dom";
import Header from "../Header/Header";
import "./BrokenLink.css";

export default function BrokenLink(props) {
    return (
        <>
            <Link to="/">
                <Header />
            </Link>
            <main>
                <h1 className="error">Oop's you were not meant to see this, you must be lost!</h1>
                <Link to="/">
                    <button>take me home</button>
                </Link>
            </main>
        </>
    );
}
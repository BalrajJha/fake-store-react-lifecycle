import React from "react";
import "./Product.css";
import { Link } from "react-router-dom";

class Product extends React.Component {
    render() {
        return (
            <Link to={`details/${this.props.id}`} className="product-card">
                <div className="image-container">
                    <img src={this.props.image} alt={this.props.title} />
                </div>

                <div className="product-card-content">
                    <h3>{this.props.title}</h3>
                    <h2>{this.props.category}</h2>
                    <div className="product-card-purchase">
                        <p>${this.props.price.toLocaleString('en-US', { style: 'currency', currency: 'USD' }).substring(1)}</p>
                        <p><i className="fa-solid fa-star"></i>{this.props.rating.rate}</p>
                    </div>
                </div>
            </Link>
        );
    }
}

export default Product;

// ({this.props.rating.count} Reviews)
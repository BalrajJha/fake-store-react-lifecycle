import './App.css';
import React from "react";
import Results from './Results/Results';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Details from './Details/Details';
import BrokenLink from './BrokenLink/BrokenLink';
import DetailFunctionHook from './Details/DetailFunctionHook';
import AddProduct from './AddProduct/AddProduct';
import axios from 'axios';
import { Navigate } from "react-router-dom";
import validator from 'validator';


class App extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      activeCategory: 0,
      isLoading: false,
      productData: [],
      selectedProductData: [],
      apiError: false,
      emptyProducts: false,
      categories: [],
      addTitle: "",
      addPrice: "",
      addDescription: "",
      addCategory: "",
      addImage: "",
      addRating: "",
      addReview: "",
      formSubmitted: false,
      showFormSuccessMessage: false,
      formErrors: {
        "titleError": "Please enter title",
        "priceError": "Please enter price e.g. 100, 10, 345 etc",
        "descriptionError": "Please enter description",
        "categoryError": "Please select category",
        "imageError": "Please enter image link e.g. https://www.image-website.jpg",
        "ratingError": "Please leave rating from 0 to 5 e.g. 1.5, 3, 4.7 etc",
        "reviewError": "Please enter the number of reviews e.g. 200, 0, 500 etc",
      },
    }

  }

  componentDidMount() {
    this.setState({
      isLoading: true,
    });

    Promise.all([axios("https://fakestoreapi.com/products"), axios("https://fakestoreapi.com/products/categories/")])
      .then((values) => {
        console.log(values[0].data, values[1].data);
        // empty product test
        // values[0].data = [];

        // values[0].data is the data of all products
        if (values[0].data.length === 0 || values[1].data.length === 0) {
          this.setState({
            emptyProducts: true,
          });
        } else {
          const categories = ["All category"];
          // values[1].data is the data of all categories
          categories.push(...values[1].data);
          console.log(values[0].data);
          // let productData = [];

          // productData = values[0].data.map((value) => {
          //   value["makeAPICall"] = true;

          //   return value;
          // });

          // console.log(productData)
          this.setState({
            productData: values[0].data,
            selectedProductData: values[0].data,
            categories: categories,
          });
        }
        this.setState({
          isLoading: false,
        });
      })
      .catch((error) => {
        this.setState({
          isLoading: false,
          emptyProducts: false,
          apiError: true,
        })
        console.error(error, ": Error in fetch");
      })
  }

  componentDidUpdate(prevProps, prevState, snapShot) {
    if (prevState.formSubmitted !== this.state.formSubmitted) {
      this.setState({
        formSubmitted: false,
      });
    }
  }

  handleCategoryChange = (event) => {
    let selectedProductData = [];

    if (event.target.value.toLowerCase() === 'All category'.toLowerCase()) {
      selectedProductData = this.state.productData;
    } else {
      selectedProductData = this.state.productData.filter((product) => {
        return event.target.value.toLowerCase() === product.category.toLowerCase();
      });
    }
    console.log(event.target.value);
    this.setState({
      selectedProductData: selectedProductData,
      activeCategory: Number(event.target.dataset.index),
    }, () => {
      console.log(this.state.selectedProductData);
      console.log(event.target.dataset.index);
    })
  }

  handleChange = (event) => {
    console.log(event.target.name, event.target.value);
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value,
    });
  }

  handleAddProduct = (event) => {
    event.preventDefault();
    let validatorCount = 0;
    const formErrors = {};

    if (validator.isAlphanumeric(validator.blacklist(this.state.addTitle, ' '))) {
      formErrors["titleError"] = "";
      validatorCount++;
    } else {
      formErrors["titleError"] = "Please enter title";
    }

    if (validator.isCurrency(this.state.addPrice)) {
      formErrors["priceError"] = "";
      validatorCount++;
    } else {
      formErrors["priceError"] = "Please enter price e.g. 100, 10, 345 etc";
    }

    if (validator.isLength(this.state.addDescription, { min: 1, max: 750 })) {
      formErrors["descriptionError"] = "";
      validatorCount++;
    } else {
      formErrors["descriptionError"] = "Please enter description";
    }

    if (this.state.addCategory) {
      formErrors["categoryError"] = "";
      validatorCount++;
    } else {
      formErrors["categoryError"] = "Please select category";
    }

    if (validator.isURL(this.state.addImage)) {
      formErrors["imageError"] = "";
      validatorCount++;
    } else {
      formErrors["imageError"] = "Please enter image link e.g. https://www.image-website.jpg";
    }

    if (validator.isFloat(this.state.addRating, { min: 0, max: 5 }) && this.state.addRating !== "-0") {
      formErrors["ratingError"] = "";
      validatorCount++;
    } else {
      formErrors["ratingError"] = "Please leave rating from 0 to 5 e.g. 1.5, 3, 4.7 etc";
    }

    if (validator.isNumeric(this.state.addReview, { no_symbols: true })) {
      formErrors["reviewError"] = "";
      validatorCount++;
    } else {
      formErrors["reviewError"] = "Please enter the number of reviews e.g. 200, 0, 500 etc";
    }

    let product = {
      "id": this.state.productData.length + 1,
      "title": this.state.addTitle.trim(),
      "price": this.state.addPrice[0] === '$' ? this.state.addPrice.substring(1) : this.state.addPrice,
      "description": this.state.addDescription.trim(),
      "category": this.state.addCategory,
      "image": this.state.addImage,
      "rating": {
        "rate": this.state.addRating,
        "count": Math.floor(this.state.addReview),
      },
    };

    let newProductData = this.state.productData.slice();
    newProductData.push(product);
    console.log(validatorCount);

    let countOfErrors = Object.values(formErrors).reduce((sum, error) => {
      sum += error.length ? 0 : 1;

      return sum;
    }, 0);

    if (countOfErrors === 7) {
      this.setState({
        productData: newProductData,
        selectedProductData: newProductData,
        activeCategory: 0,
        formSubmitted: true,
        addTitle: "",
        addPrice: "",
        addDescription: "",
        addCategory: "",
        addImage: "",
        addRating: "",
        addReview: "",
        formErrors: {
          "titleError": "Please enter title",
          "priceError": "Please enter price e.g. 100, 10, 345 etc",
          "descriptionError": "Please enter description",
          "categoryError": "Please select category",
          "imageError": "Please enter image link e.g. https://www.image-website.jpg",
          "ratingError": "Please leave rating from 0 to 5 e.g. 1.5, 3, 4.7 etc",
          "reviewError": "Please enter the number of reviews e.g. 200, 0, 500 etc",
        },
      });
    } else {
      this.setState({
        formSubmitted: false,
        formErrors: formErrors,
      });
    }
  }

  render() {
    return (
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Results handleCategoryChange={this.handleCategoryChange}
            {...this.state} />} />
          <Route path="/details/:id" element={<DetailFunctionHook productData={this.state.productData} />} />

          <Route path="/add" element={<AddProduct handleChange={this.handleChange}
            categories={this.state.categories}
            handleAddProduct={this.handleAddProduct}
            formSubmitted={this.state.formSubmitted}
            formErrors={this.state.formErrors}
          />} />

          <Route path="*" element={<BrokenLink />} />
        </Routes>
      </BrowserRouter>
    );
  }
}

export default App;



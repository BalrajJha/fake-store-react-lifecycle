import React from "react";
import "./Categories.css";

export default function Categories(props) {
    return (
        <div className="categories" >
            {props.categories.map((category, index) => {
                return (
                    <button key={category}
                        value={category}
                        onClick={props.onClick}
                        className={index === props.activeCategory ? "activeCategory" : ""}
                        data-index={index}
                    >
                        {category}
                    </button>
                );
            })
            }
        </div >
    );
}

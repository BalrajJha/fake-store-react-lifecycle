import React from "react";
import "./AddProduct.css";
import Header from "../Header/Header";
import { Link, Navigate } from "react-router-dom";
import Logo from "./logo.png";

class AddProduct extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <>
                <Link to="/">
                    <Header hideAddProduct="true" />
                </Link>

                {this.props.formSubmitted ?
                    <Navigate to="/" /> :
                    null
                }
                <div className="main-add-product">
                    <form onSubmit={this.props.handleAddProduct}>
                        <h1>Add Products</h1>
                        <label>
                            <input type="text"
                                name="addTitle"
                                value={this.props.addTitle}
                                onChange={this.props.handleChange}
                                placeholder="Title"
                            >
                                {this.props.addTitle}
                            </input>
                        </label>
                        <p>{this.props.formErrors["titleError"] ? this.props.formErrors["titleError"] : <span>&nbsp;</span>}</p>

                        <label>
                            <input type="text"
                                name="addPrice"
                                value={this.props.addPrice}
                                onChange={this.props.handleChange}
                                placeholder="Price"
                            >
                                {this.props.addPrice}
                            </input>
                            <p>{this.props.formErrors["priceError"] ? this.props.formErrors["priceError"] : <span>&nbsp;</span>}</p>
                        </label>

                        <label>
                            <textarea
                                name="addDescription"
                                value={this.props.addDescription}
                                onChange={this.props.handleChange}
                                placeholder="Description"
                            >
                                {this.props.addDescription}
                            </textarea>
                            <p>{this.props.formErrors["descriptionError"] ? this.props.formErrors["descriptionError"] : <span>&nbsp;</span>}</p>
                        </label>

                        <label>
                            <select name="addCategory"
                                value={this.props.addCategory}
                                onChange={this.props.handleChange}
                            >
                                <option value="" selected disabled hidden>Select category</option>
                                {
                                    this.props.categories.filter((category) => {
                                        return category.toLowerCase() !== 'All category'.toLowerCase();
                                    }).map((category) => {
                                        return (<option key={category} value={category}>{category}</option>);
                                    })
                                }
                            </select>
                            <p>{this.props.formErrors["categoryError"] ? this.props.formErrors["categoryError"] : <span>&nbsp;</span>}</p>
                        </label>

                        <label>
                            <input type="text"
                                name="addImage"
                                value={this.props.addImage}
                                onChange={this.props.handleChange}
                                placeholder="Image Link"
                            >
                                {this.props.addImage}
                            </input>
                        </label>
                        <p>{this.props.formErrors["imageError"] ? this.props.formErrors["imageError"] : <span>&nbsp;</span>}</p>

                        <label>
                            <input type="text"
                                name="addRating"
                                value={this.props.addRating}
                                onChange={this.props.handleChange}
                                placeholder="Rating"
                            >
                                {this.props.addRating}
                            </input>
                            <p>{this.props.formErrors["ratingError"] ? this.props.formErrors["ratingError"] : <span>&nbsp;</span>}</p>
                        </label>

                        <label>
                            <input type="text"
                                name="addReview"
                                value={this.props.addReview}
                                onChange={this.props.handleChange}
                                placeholder="Number of reviews"
                            >
                                {this.props.addReview}
                            </input>
                            <p>{this.props.formErrors["reviewError"] ? this.props.formErrors["reviewError"] : <span>&nbsp;</span>}</p>
                        </label>
                        <button type="submit">Add product</button>
                    </form>
                    <div className="form-image-container">

                        <img src={Logo} alt="logo" />
                    </div>
                </div>
            </>
        );
    }
}

export default AddProduct;
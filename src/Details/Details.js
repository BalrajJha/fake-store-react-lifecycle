import React from "react";
import Header from "../Header/Header";
import axios from "axios";
import { Link, Navigate } from "react-router-dom";
import Loader from "../Loader/Loader";
import "./Details.css";

class Details extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            // isLoading: false,
            noProducts: false,
            // apiError: false,
            title: "",
            price: "",
            description: "",
            category: "",
            image: "",
            rate: "",
            count: "",
            fullStar: 0,
            remainingStar: 0,
        }

    }

    // static getDerivedStateFromProps(props, state) {
    //     if (props.productData === undefined) {
    //         <Navigate to="/" replace={true} />
    //     }

    // }

    componentDidMount() {
        // don't use this as this is DOM, also not a good way to get params
        // const id = window.location.href.substring(window.location.href.lastIndexOf("/") + 1);

        // console.log("IN DETAIL", this.props.productData);
        // this wont't work in componentDidMount as rendered has already been called once
        // thinnking of using getDerivedStateFromProps for this one
        // if (this.props.productData == undefined) {
        //     <Navigate to="/" replace={true} />
        // }

        if (this.props.productData !== undefined) {
            this.setState({
                title: this.props.productData.title,
                price: this.props.productData.price,
                description: this.props.productData.description,
                category: this.props.productData.category,
                image: this.props.productData.image,
                rate: this.props.productData.rating.rate,
                count: this.props.productData.rating.count,
            }, this.calculateStarRatings);
        }
    }

    calculateStarRatings = () => {
        const rating = isNaN(this.state.rate) ? 0 : Number(this.state.rate);
        const fullStar = Math.floor(rating);
        const remainingStar = 5 - fullStar;

        this.setState({
            fullStar: fullStar,
            remainingStar: remainingStar,
        });
    }

    render() {
        console.log("INS IN OUTS OF DETAILS");
        return (
            <>
                <Link to="/">
                    <Header />
                </Link>

                {this.state.noProducts ?
                    <h2 className="error">Sorry we can't show the product now, please come back later</h2>
                    :
                    this.props.productData === undefined ?
                        <Navigate to="/" replace={true} /> :
                        <main className="product-details">

                            <div className="product-detail-image-container">
                                <img src={this.state.image} alt={this.state.title} />
                            </div>

                            <div className="product-detail">
                                <h2 className="product-detail-title">{this.state.title}</h2>

                                <h3 className="product-detial-rating">
                                    {new Array(this.state.fullStar).fill(0).map((star, index) => {
                                        return (<i className="fa-solid fa-star" key={index}></i>);
                                    })
                                    }
                                    {new Array(this.state.remainingStar).fill(0).map((star, index) => {
                                        return (<i className="fa-regular fa-star" key={index}></i>);
                                    })
                                    } &nbsp; <span className="product-detail-reviews">{this.state.count} Reviews</span>
                                </h3>

                                <h3 className="product-detail-category">{this.state.category}</h3>
                                <h3 className="product-detail-description">{this.state.description}</h3>

                                <div className="product-detail-purchasing">
                                    <h3 className="product-detial-price"><sup>$</sup>{this.state.price.toLocaleString('en-us', { style: 'currency', currency: 'USD' }).substring(1)}</h3>
                                    <button className="product-detail-cart"><i className="fa-solid fa-cart-arrow-down"></i>Add to Cart</button>
                                </div>

                            </div>
                        </main>
                }
            </>
        );
    }
}

export default Details;

// No need to make api calls as storing state in App and passing productDetail as props
// but still keeping this here

// makeAPICall() {
//     this.setState({
//         isLoading: true,
//     });

//     axios(`https://fakestoreapi.com/products/${this.props.id}`)
//         .then((response) => {
//             let product = response.data;
//             console.log(JSON.stringify(product));
//             // empty product test
//             // product = {};
//             if (Object.keys(product).length === 0) {
//                 this.setState({
//                     noProducts: true,
//                 });
//             } else {
//                 this.setState({
//                     title: response.data.title,
//                     price: response.data.price,
//                     description: response.data.description,
//                     category: response.data.category,
//                     image: response.data.image,
//                     rate: response.data.rating.rate,
//                     count: response.data.rating.count,
//                 }, this.calculateStarRatings);
//             }

//             this.setState({
//                 isLoading: false,
//             });
//         })
//         .catch((error) => {
//             console.error("Error: ", error);
//             this.setState({
//                 isLoading: false,
//                 apiError: true,
//             });
//         })
// }

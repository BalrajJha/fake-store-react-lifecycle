import React from "react";
import { useParams } from 'react-router-dom';
import Details from "./Details";

function DetailFunctionHook(props) {
    //get the details id param from the URL
    let { id } = useParams();
    console.log("IN funciotn hook");
    return (
        // look at props.productData[id] here
        // passing only single product instead of passing everything
        // TODO: find a way to pass only single product from the start, why pass everything in the first place
        // I have the selected id but how to pass it in App.js
        <Details id={id} productData={props.productData[id - 1]} />
    )
}

export default DetailFunctionHook;
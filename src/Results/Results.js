import axios from "axios";
import React from "react";
import "./Results.css";
import Loader from "../Loader/Loader";
import Product from "../Product/Product";
import Header from "../Header/Header";
import Categories from "../Categories/Categories";

class Results extends React.Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <main className="product-results">
                <Header handleAddProduct={this.handleAddProduct} />

                {this.props.isLoading ? <Loader /> : null}

                {this.props.apiError ?
                    <h2 className="error">Sorry we couldn't connect to our servers, please try again later</h2>
                    :
                    null
                }


                {this.props.emptyProducts ?
                    <h2 className="error">Sorry we have run out of products to show, please come back later</h2>
                    :
                    <>
                        <Categories onClick={this.props.handleCategoryChange}
                            categories={this.props.categories}
                            activeCategory={this.props.activeCategory}
                        />

                        {/* 
                                lifting state up
                                <div className="categories">
                                <button onClick={this.handleCategoryChange} value="All category">All category</button>
                                <button onClick={this.handleCategoryChange}>electronics</button>
                                <button onClick={this.handleCategoryChange}>jewelery</button>
                                <button onClick={this.handleCategoryChange}>men's clothing</button>
                                <button onClick={this.handleCategoryChange}>women's clothing</button>
                            </div> */}

                        <div className="product-data">
                            {
                                this.props.selectedProductData.map((product) => {
                                    return (
                                        <Product key={product.id}
                                            id={product.id}
                                            image={product.image}
                                            category={product.category}
                                            title={product.title}
                                            price={product.price}
                                            rating={product.rating}
                                        />
                                    );
                                })
                            }
                        </div>
                    </>
                }
            </main>
        );
    }
}

export default Results;
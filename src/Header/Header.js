import React from "react";
import { Link } from "react-router-dom";
import "./Header.css";
class Header extends React.Component {
    constructor(props) {
        super(props);

    }
    render() {
        return (
            <header>
                <h1><span className="header-secondary">Boutique</span> shop</h1>
                {!this.props.hideAddProduct ?
                    <Link to="/add">
                        <button className="add-product">Add Product</button>
                    </Link> :
                    null
                }

            </header>
        );
    }
}

export default Header;